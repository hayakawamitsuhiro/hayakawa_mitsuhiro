<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ユーザー登録</title>
</head>
<body>
<div class="top">
			<p class="HOME">ユーザー登録</p>
			<p class="youkoso">ようこそ${loginUser.name}さん</p>
			<div class="toplink">
			<a href="usermanagement">ユーザー管理へ戻る</a>
			</div>
		</div>
	<div class="main-contens">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<div class="signupfactor">
		<form action="signup" method="post" >
		<table>
		<tr>
			<th><label for="login_id">ログインID(半角英数字、6文字以上20文字以下)</label><label for="hissu" class="hissu">必須</label></th>
			<td><input name="login_id" value="${user.login_id}" id="login_id"></td>
		</tr>
		<tr>
			<th><label for="password">パスワード(半角文字、6文字以上20文字以下)</label><label for="hissu" class="hissu">必須</label></th>
			<td><input name="password" type="password" id="password"></td>
		</tr>
		<tr>
			<th><label for="confirmPassword">パスワード再確認</label></th>
			<td><input name="confirmPassword" type="password" id="confirmPasswod"></td>
		</tr>
		<tr>
			<th><label for="name">名前(10文字以下)</label><label for="hissu" class="hissu">必須</label></th>
			<td><input name="name" value="${user.name}" id="name"></td>
		</tr>
		<tr>
			<th><label for="branch_office">所属支店</label><br><label for="branch_office" class="setsumei">本社の場合所属部署・役職は総務以外選択できません</label></th>
			<td><select name="branch_office" id="branch_office">
			<c:forEach items="${offices}" var="office">
				<option value="${office.branch_office}">${office.office_name}</option>
			</c:forEach>
			</select>
			</td>
		</tr>
		<tr>
			<th><label for="department">所属部署・役職</label><br><label for="branch_office" class="setsumei">総務の場合所属支店は本社以外選択できません</label></th>
			<td><select name="department" id="department">
			<c:forEach items="${departments}" var="department">
				<option value="${department.department}">${department.department_name}</option>
			</c:forEach>
			</select></td>
		</tr>
		<tr>
			<th>登録</th>
			<td><input type="submit" value="登録" /></td>
		</tr>
		</table>
		</form>
		</div>
	</div>
</body>
</html>