<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css"
	rel="stylesheet">
<script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
<title>HOME</title>
<script type="text/javascript">
	function commentremovecheck() {

		if (window.confirm('コメントを削除します')) { // 確認ダイアログを表示
			return true; // 「OK」時は送信を実行
		} else { // 「キャンセル」時の処理
			return false; // 送信を中止
		}
	}

	function postremovecheck() {

		if (window.confirm('投稿を削除します')) { // 確認ダイアログを表示
			return true; // 「OK」時は送信を実行
		} else { // 「キャンセル」時の処理
			return false; // 送信を中止
		}
	}
</script>
</head>
<body>
	<div class="main-contains">
		<div class="top">
			<p class="HOME">HOME</p>
			<p class="youkoso">ようこそ${loginUser.name}さん</p>
			<div class="toplink">
				<c:if test="${ not empty loginUser}">
					<c:if
						test="${loginUser.branch_office == 1 && loginUser.department == 1}">
						<a href="logout" class="square_btn">ログアウト</a>
						<a href="newpost" class="square_btn">新規投稿</a>
						<a href="usermanagement" class="square_btn">ユーザー管理</a>
					</c:if>
					<c:if
						test="${loginUser.branch_office != 1 && loginUser.department != 1}">
						<a href="logout" class="square_btn">ログアウト</a>
						<a href="newpost" class="square_btn">新規投稿</a>
					</c:if>
				</c:if>
			</div>
		</div>
		<c:if test="${ not empty loginUser}">
			<div class="search">
				<form action="./" method="get">
					<label for="searchCategory" class="searchmoji">カテゴリー検索</label> <input
						name="searchCategory" id="searchCategory"
						value="${searchpost.search_category}"> <label
						for="searchDate" class="searchmoji">投稿日付</label> <input
						name="searchDate1" type="date" id="searchDate1"
						value="${searchpost.created_date1}" class="searchmoji1"> ～
					<input name="searchDate2" type="date" id="searchDate2"
						value="${searchpost.created_date2}" class="searchmoji2"> <input
						type="submit" value="絞込み" class="searchCategory">
				</form>
				<form action="./" method="get">
					<button type="submit" value="" class="searchDate">絞込み解除</button>
				</form>
			</div>
			<div class="topmain">
			<c:if test="${ empty newposts }">
				<p class="nonpost">投稿がありません</p>
			</c:if>
				<c:if test="${ not empty errorMessages }">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${errorMessages}" var="message">
								<li><c:out value="${message}" />
							</c:forEach>
						</ul>
					</div>
					<c:remove var="errorMessages" scope="session" />
				</c:if>
				<c:if test="${ not empty newposts }">
					<c:forEach items="${newposts}" var="newpost">
						<div class="toppost">
							<div class="csubject">
								<span class="category"><c:out
										value="カテゴリー：${newpost.category}" /></span><br> <span
									class="subject"><c:out value="件名：${newpost.subject}" /></span><br>
							</div>
							<div class="letterbody">
								<span class="lbody">本文</span><br>
								<c:forEach var="str"
									items="${ fn:split(newpost.letter_body,'
					') }">
									<span class="letter_body"><c:out value="${str}" /></span>
									<br>
								</c:forEach>
							</div>
							<div class="postuserdate">
								投稿した人：<span class="name"><c:out value="${newpost.name}" /></span><br>
								<div class="date">
									投稿日時：<fmt:formatDate value="${newpost.created_date}"
										pattern="yyyy/MM/dd HH:mm:ss" />
								</div>
								<form action="removepost" method="post"
									onSubmit="return  postremovecheck()">
									<c:if test="${loginUser.user == newpost.user}">
										<input type="hidden" name="post_id" value="${newpost.post_id}">
										<input type="submit" value="投稿を削除">
									</c:if>
									<br>
								</form>
							</div>
							<form action="comment" method="post">
								<label for="comment">コメント入力(500文字以下)</label><br> <input
									type="hidden" name="post_id" value="${newpost.post_id}" />
								<c:choose>
									<c:when test="${newpost.post_id == errcomment.post_id}">
										<textarea name="comment" rows="2" cols="50">${errcomment.comment}</textarea>
										<c:remove var="errcomment" scope="session" />
									</c:when>
									<c:when test="${newpost.post_id != errcomment.post_id}">
										<textarea name="comment" rows="2" cols="50"></textarea>
									</c:when>
								</c:choose>
								<input type="submit" value="コメント">
							</form>
							<c:forEach items="${comments}" var="comment">
								<c:if test="${ newpost.post_id == comment.post_id }">
									<div class="balloon4">
										<c:forEach var="spcomment"
											items="${ fn:split(comment.comment,'
									') }">
											<span class="comment"><c:out value="${spcomment}" /></span>
											<br>
										</c:forEach>
										<div class="commentuserdate">
											コメントした人:
											<c:out value="${comment.name}"></c:out>
											<br> コメント日時：
											<fmt:formatDate value="${comment.created_date}"
												pattern="yyyy/MM/dd HH:mm:ss" />
											<form action="removecomment" method="post"
												onSubmit="return  commentremovecheck()">
												<c:if test="${loginUser.user == comment.user}">
													<input type="hidden" name="comment_id"
														value="${comment.comment_id}">
													<input type="submit" value="コメントを削除">
												</c:if>
												<br>
											</form>
										</div>
									</div>
								</c:if>
							</c:forEach>
						</div>
					</c:forEach>
				</c:if>
			</div>
		</c:if>
	</div>
</body>
</html>