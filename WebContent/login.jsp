<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ログイン</title>
</head>
<body>
<div class="login">
	<h1 class="loginmoji">ログイン</h1>
</div>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
	<div class="loginfactor">
		<form action="login" method="post">
			<label for="login_id">ログインID</label>
			<input type="text" name="login_id" id="login_id" autocomplete="login_id"/> <br />
			<br>
			<label for="password">パスワード</label>
			<input name="password" type="password" id="password" /> <br />
			<br>
			<input type="submit" value="ログイン" /> <br />
		</form>
	</div>
	<div class="copyright">Copyright(c)Hayakawa.Mitsuhiro</div>
	</div>
</body>
</html>