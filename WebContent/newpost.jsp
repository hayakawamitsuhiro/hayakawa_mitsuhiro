<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>新規投稿</title>
</head>
<body>
	<div class="main-contents">
	<div class="top">
			<p class="HOME">新規投稿</p>
			<p class="youkoso">ようこそ${loginUser.name}さん</p>
			<div class="toplink">
			<a href="./">HOMEへ戻る</a>
			</div>
		</div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<div class="signupfactor">
		<form action="newpost" method="post">
		<table>
		<tr>
			<th><label for="category">カテゴリー(10文字以下)</label><label for="hissu" class="hissu">必須</label></th>
			<td><input name="category" value="${errorpost.category}" id="category"></td>
		</tr>
		<tr>
			<th><label for="subject">件名(30文字以下)</label><label for="hissu" class="hissu">必須</label></th>
			<td><input name="subject" value="${errorpost.subject}" id="subject" class="sujecttextbox"></td>
		</tr>
		<tr>
			<th><label for="letter_body">本文(1000文字以下)</label><label for="hissu" class="hissu">必須</label></th>
			<td><textarea name="letter_body" rows="5" cols="50">${errorpost.letter_body}</textarea></td>
		</tr>
		<tr>
			<th>投稿</th>
			<td><input type="submit" value="投稿"></td>
		</tr>
		</table>
		</form>
		</div>
	</div>

</body>
</html>