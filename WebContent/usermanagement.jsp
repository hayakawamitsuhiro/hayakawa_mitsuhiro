<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ユーザー管理</title>
<script type="text/javascript">
	function stopcheck() {

		if (window.confirm('アカウントを停止します')) { // 確認ダイアログを表示
			return true; // 「OK」時は送信を実行
		} else { // 「キャンセル」時の処理
			return false; // 送信を中止
		}
	}
	function restorationcheck() {
		if (window.confirm('アカウント停止を解除します')) { // 確認ダイアログを表示
			return true; // 「OK」時は送信を実行
		} else { // 「キャンセル」時の処理
			return false; // 送信を中止
		}
	}
	function removecheck() {
		if (window.confirm('アカウントを削除します')) { // 確認ダイアログを表示
			return true; // 「OK」時は送信を実行
		} else { // 「キャンセル」時の処理
			return false; // 送信を中止
		}
	}
</script>
</head>
<body>
	<div class="top">
		<p class="HOME">ユーザー管理</p>
		<p class="youkoso">ようこそ${loginUser.name}さん</p>
		<div class="toplink">
			<a href="./" class="square_btn">HOMEへ戻る</a> <a href="signup"
				class="square_btn">ユーザー登録</a>
		</div>
	</div>
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>
	<div class="managementfactor">
		<table class="managementtable">
			<tr>
				<th>氏名</th>
				<th>ログインID</th>
				<th>所属支店</th>
				<th>所属部署</th>
				<th>ステータス</th>
				<th>編集</th>
				<th>削除</th>
			</tr>
			<c:forEach items="${userList}" var="users">
				<tr>
					<td><c:out value="${users.name}"></c:out></td>
					<td><c:out value="${users.login_id}"></c:out></td>
					<td><c:out value="${users.office_name}"></c:out></td>
					<td><c:out value="${users.department_name}"></c:out></td>
					<td><c:if test="${users.stop_flag == 1 }">
							<form action="userrestoration" method="post"
								onSubmit="return  restorationcheck()">
								<input type="hidden" name="user" value="${users.user}">
								<input type="submit" id="rebtn" value="停止解除" class="rebtn">
							</form>
						</c:if> <c:if
							test="${users.stop_flag == 0 && loginUser.user != users.user}">
							<form action="userstop" method="post"
								onSubmit="return stopcheck()">
								<input type="hidden" name="user" value="${users.user}">
								<input type="submit" id="stopbtn" value="停止" class="stopbtn">
							</form>
						</c:if> <c:if
							test="${users.stop_flag == 0 && loginUser.user == users.user}">
						ログイン中
				</c:if></td>
					<td>
						<form action="useredit" method="get">
							<input type="hidden" name="user" value="${users.user}"> <input
								type="submit" value="編集">
						</form>
					</td>
					<td><c:if test="${loginUser.user != users.user}">
							<form action="userremove" method="post"
								onSubmit="return  removecheck()">
								<input type="hidden" name="user" value="${users.user}">
								<input type="submit" value="削除" class="remove">
							</form>
						</c:if> <c:if test="${loginUser.user == users.user}">
					ログイン中
				</c:if></td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>