package hayakawa_mitsuhiro.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hayakawa_mitsuhiro.service.RemovePostService;

@WebServlet("/removepost")
public class RemovePost extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		int comment = Integer.parseInt(request.getParameter("post_id"));

		new RemovePostService().delete(comment);
		response.sendRedirect("./");
	}
}