package hayakawa_mitsuhiro.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import hayakawa_mitsuhiro.beans.CommentBeans;
import hayakawa_mitsuhiro.beans.UserBeans;
import hayakawa_mitsuhiro.service.CommentService;


@WebServlet("/comment")
public class Comment extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		UserBeans user = (UserBeans) session.getAttribute("loginUser");
		CommentBeans comment = new CommentBeans();
		List<String> messages = new ArrayList<String>();

		comment.setPost_id(Integer.parseInt(request.getParameter("post_id")));
		comment.setComment(request.getParameter("comment"));
		comment.setUser(user.getUser());

		String comments = comment.getComment();
		if (StringUtils.isBlank(comments) == true) {
			messages.add("コメントを入力してください");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
			return;
		} else if (comments.length() > 500){
			messages.add("コメントは500文字以下で入力してください");
			session.setAttribute("errcomment", comment);
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
		} else {
		new CommentService().register(comment);
		response.sendRedirect("./");
		}
	}
}
