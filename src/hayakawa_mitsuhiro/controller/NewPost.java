package hayakawa_mitsuhiro.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import hayakawa_mitsuhiro.beans.NewPostBeans;
import hayakawa_mitsuhiro.beans.UserBeans;
import hayakawa_mitsuhiro.service.NewPostService;

@WebServlet("/newpost")



public class NewPost extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("newpost.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();
		UserBeans user = (UserBeans) session.getAttribute("loginUser");
		NewPostBeans message = new NewPostBeans();

		message.setSubject(request.getParameter("subject"));
		message.setLetter_body(request.getParameter("letter_body"));
		message.setCategory(request.getParameter("category"));
		message.setUser(user.getUser());

		if (isValid(request, messages) == true) {
			new NewPostService().register(message);
			session.setAttribute("newpost", message);
			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("errorpost", message);
			request.getRequestDispatcher("newpost.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String subject = request.getParameter("subject");
		String letterBody = request.getParameter("letter_body");
		String category = request.getParameter("category");

		if (StringUtils.isBlank(category) == true) {
			messages.add("カテゴリーを入力してください");
		}
		if (10 < category.length()) {
			messages.add("カテゴリーは10文字以下で入力してください");
		}
		if (StringUtils.isBlank(subject) == true) {
			messages.add("件名を入力してください");
		}
		if (30 < subject.length()) {
			messages.add("件名は30文字以下で入力してください");
		}
		if (StringUtils.isBlank(letterBody) == true) {
			messages.add("本文を入力してください");
		}
		if (1000 < letterBody.length()) {
			messages.add("本文は1000文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
