package hayakawa_mitsuhiro.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import hayakawa_mitsuhiro.beans.UserBeans;
import hayakawa_mitsuhiro.service.UserManagementService;


@WebServlet("/usermanagement")
public class UserManagement extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		List<UserBeans> user = new UserManagementService().userList();

		if (isValid(request, messages) == true) {
			session.setAttribute("userList", user);
			request.getRequestDispatcher("usermanagement.jsp").forward(request, response);
		} else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
		}
	}
	private boolean isValid(HttpServletRequest request, List<String> messages) {
		HttpSession session = request.getSession();
		UserBeans loginUser = (UserBeans) session.getAttribute("loginUser");

		if (loginUser.getBranch_office() !=1 && loginUser.getDepartment() != 1) {
			messages.add("アクセス権限がありません");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
