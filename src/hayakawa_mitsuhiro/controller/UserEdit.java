package hayakawa_mitsuhiro.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import hayakawa_mitsuhiro.beans.UserBeans;
import hayakawa_mitsuhiro.service.DepartmentService;
import hayakawa_mitsuhiro.service.OfficeService;
import hayakawa_mitsuhiro.service.OverlapService;
import hayakawa_mitsuhiro.service.UserEditService;

/**
 * Servlet implementation class UserEdit
 */
@WebServlet("/useredit")
public class UserEdit extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String stuser = request.getParameter("user");
		List<UserBeans> department = new DepartmentService().departmentList();
		List<UserBeans> office = new OfficeService().officeList();
		HttpSession session = request.getSession();
		UserBeans userList = new OverlapService().userList(stuser);
		List<String> messages = new ArrayList<String>();

		if ((stuser.matches("[0-9]+"))) {
			int userid = Integer.parseInt(stuser);
			if (userList != null){
				UserBeans userEdit = new UserEditService().user(userid);
				session.setAttribute("departments", department);
				session.setAttribute("offices", office);

				session.setAttribute("editUsers", userEdit);
				request.getRequestDispatcher("useredit.jsp").forward(request, response);
			} else {
				messages.add("不正なパラメータが入力されました");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("usermanagement");
			}
		} else {
			messages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("usermanagement");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<String> messages = new ArrayList<String>();
		UserBeans edituser = new UserBeans();
		HttpSession session = request.getSession();

		edituser.setLogin_id(request.getParameter("login_id"));
		edituser.setConfirmPassword(request.getParameter("confirmPassword"));
		edituser.setName(request.getParameter("name"));
		edituser.setBranch_office(Integer.parseInt(request.getParameter("branch_office")));
		edituser.setDepartment(Integer.parseInt(request.getParameter("department")));
		edituser.setUser(Integer.parseInt(request.getParameter("user")));
		edituser.setPassword(request.getParameter("password"));

		if (isValid(request, messages) == true) {
			session.setAttribute("editUser", edituser);
			new UserEditService().edituser(edituser);
			response.sendRedirect("usermanagement");
			return;
		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUsers", edituser);
			request.getRequestDispatcher("useredit.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		int editId = Integer.parseInt(request.getParameter("user"));
		String loginId = request.getParameter("login_id");
		String pass = request.getParameter("password");
		String conpass = request.getParameter("confirmPassword");
		String name = request.getParameter("name");
		UserBeans userId = new OverlapService().List(loginId);
		int department =Integer.parseInt(request.getParameter("department"));
		int office =Integer.parseInt(request.getParameter("branch_office"));

		if (StringUtils.isBlank(loginId) == true) {
			messages.add("ログインIDを入力してください");
		} else if (loginId.length() > 20 || loginId.length() < 6) {
			messages.add("ログインIDは6文字以上20文字以下で入力してください");
		} else if (loginId.matches("\\W+")) {
			messages.add("ログインIDは半角英数字で入力してください");
		}
		if (userId != null) {
			if (userId.getUser() != editId) {
				messages.add("ログインIDが重複しています");
			}
		}
		if (!(pass.equals(conpass))){
			messages.add("パスワードが一致していません");
		} else if (pass.matches("^[^a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]+$")){
			messages.add("パスワードは記号を含む半角英数字で入力してください");
		}
		if (StringUtils.isEmpty(pass)) {
		}
		else if (pass.length() > 20 || pass.length() < 6) {
			messages.add("パスワードは6文字以上20文字以下で入力してください");
		}
		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		}
		if (name.length() > 10) {
			messages.add("名前は10文字以下で入力してください");
		}
		if(department == 1 && office != 1) {
			messages.add("所属支店と所属部署・役職の組み合わせが不正です");
		}
		if(department != 1 && office == 1) {
			messages.add("所属支店と所属部署・役職の組み合わせが不正です");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
