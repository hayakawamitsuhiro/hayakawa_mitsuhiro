package hayakawa_mitsuhiro.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import hayakawa_mitsuhiro.beans.UserBeans;
import hayakawa_mitsuhiro.service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		String loginId = request.getParameter("login_id");
		String password = request.getParameter("password");

		LoginService loginService = new LoginService();
		UserBeans users = loginService.login(loginId, password);



		if (users != null && users.getStop_flag() == 0) {
			session.setAttribute("loginUser", users);
			response.sendRedirect("./");
		} else if (users != null && users.getStop_flag() == 1) {
			messages.add("ログイン出来ません");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("login");
		} else {
			messages.add("ログインIDかパスワードが間違っています");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("login");
		}
	}

}