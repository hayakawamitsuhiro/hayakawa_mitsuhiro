package hayakawa_mitsuhiro.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import hayakawa_mitsuhiro.beans.UserBeans;
import hayakawa_mitsuhiro.service.DepartmentService;
import hayakawa_mitsuhiro.service.OfficeService;
import hayakawa_mitsuhiro.service.OverlapService;
import hayakawa_mitsuhiro.service.UserService;

@WebServlet("/signup")
public class SignUp extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<UserBeans> department = new DepartmentService().departmentList();
		List<UserBeans> office = new OfficeService().officeList();
		HttpSession session = request.getSession();
		session.setAttribute("departments", department);
		session.setAttribute("offices", office);
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<String> messages = new ArrayList<String>();
		UserBeans user = new UserBeans();
		HttpSession session = request.getSession();

		user.setLogin_id(request.getParameter("login_id"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranch_office(Integer.parseInt(request.getParameter("branch_office")));
		user.setDepartment(Integer.parseInt(request.getParameter("department")));

		if (isValid(request, messages) == true) {
			session.setAttribute("users", user);
			new UserService().register(user);
			response.sendRedirect("usermanagement");
		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("user", user);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String loginId = request.getParameter("login_id");
		String pass = request.getParameter("password");
		String conpass = request.getParameter("confirmPassword");
		String name = request.getParameter("name");
		int department =Integer.parseInt(request.getParameter("department"));
		int office =Integer.parseInt(request.getParameter("branch_office"));
		UserBeans userId = new OverlapService().List(loginId);

		if (StringUtils.isBlank(loginId) == true) {
			messages.add("ログインIDを入力してください");
		} else if (loginId.length() > 20 || loginId.length() < 6) {
			messages.add("ログインIDは6文字以上20文字以下で入力してください");
		} else if (loginId.matches("\\W+")) {
			messages.add("ログインIDは半角英数字で入力してください");
		}
		if (userId != null) {
			messages.add("ログインIDが重複しています");
		}
		if (StringUtils.isBlank(pass) == true) {
			messages.add("パスワードを入力してください");
		} else if (!(pass.equals(conpass))){
			messages.add("パスワードが一致していません");
		} else if (pass.length() > 20 || pass.length() < 6) {
			messages.add("パスワードは6文字以上20文字以下で入力してください");
		} else if (pass.matches("^[^a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]+$")){
			messages.add("パスワードは記号を含む半角英数字で入力してください");
		}
		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		}
		if (name.length() > 10) {
			messages.add("名前は10文字以下で入力してください");
		}
		if(department == 1 && office != 1) {
			messages.add("所属支店と所属部署・役職の組み合わせが不正です");
		}
		if(department != 1 && office == 1) {
			messages.add("所属支店と所属部署・役職の組み合わせが不正です");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
