package hayakawa_mitsuhiro.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hayakawa_mitsuhiro.beans.CommentBeans;
import hayakawa_mitsuhiro.beans.SearchBeans;
import hayakawa_mitsuhiro.beans.UserPostBeans;
import hayakawa_mitsuhiro.service.CommentService;
import hayakawa_mitsuhiro.service.NewPostService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class Top extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		SearchBeans searchPost = new SearchBeans();


		String searchDate1 = request.getParameter("searchDate1");
		String searchDate2 = request.getParameter("searchDate2");
		String searchCategory = request.getParameter("searchCategory");

		searchPost.setCreated_date1(searchDate1);
		searchPost.setCreated_date2(searchDate2);
		searchPost.setSearch_category(searchCategory);


		List<UserPostBeans> messages = new NewPostService().getMessage(searchDate1, searchDate2, searchCategory);
		List<CommentBeans> comment = new CommentService().getMessage();

		request.setAttribute("searchpost", searchPost);
		request.setAttribute("newposts", messages);
		request.setAttribute("comments", comment);

		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}
}

