package hayakawa_mitsuhiro.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hayakawa_mitsuhiro.service.UserRestorationService;

@WebServlet("/userrestoration")
public class UserRestoration extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int userId = Integer.parseInt(request.getParameter("user"));

		new UserRestorationService().restoration(userId);;
		response.sendRedirect("usermanagement");
	}

}
