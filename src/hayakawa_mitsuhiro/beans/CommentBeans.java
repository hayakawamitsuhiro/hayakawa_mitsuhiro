package hayakawa_mitsuhiro.beans;

import java.io.Serializable;
import java.util.Date;

public class CommentBeans implements Serializable {
	private static final long serialVersionUID = 1L;

	private String comment;
	private Date created_date;
	private Date updated_date;
	private String name;
	private int user;
	private int comment_id;
	private int post_id;
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date createdDate) {
		this.created_date = createdDate;
	}
	public Date getUpdatedDate() {
		return updated_date;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updated_date = updatedDate;
	}
	public int getUser() {
		return user;
	}
	public void setUser(int user) {
		this.user = user;
	}
	public int getComment_id() {
		return comment_id;
	}
	public void setComment_id(int comment_id) {
		this.comment_id = comment_id;
	}
	public int getPost_id() {
		return post_id;
	}
	public void setPost_id(int post_id) {
		this.post_id = post_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}