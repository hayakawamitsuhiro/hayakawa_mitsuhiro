package hayakawa_mitsuhiro.beans;

import java.io.Serializable;
import java.util.Date;

public class UserPostBeans implements Serializable {
	private static final long serialVersionUID = 1L;

	private String login_id;
	private String name;
	private int user;
	private Date created_date;
	private Date update_date;
	private String subject;
	private String letter_body;
	private String category;
	private int post_id;

	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getUser() {
		return user;
	}
	public void setUser(int user) {
		this.user = user;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date createdDate) {
		this.created_date = createdDate;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getLetter_body() {
		return letter_body;
	}
	public void setLetter_body(String letter_body) {
		this.letter_body = letter_body;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public int getPost_id() {
		return post_id;
	}
	public void setPost_id(int postId) {
		this.post_id = postId;
	}
	public Date getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}


}