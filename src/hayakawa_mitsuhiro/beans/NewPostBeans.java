package hayakawa_mitsuhiro.beans;

import java.io.Serializable;
import java.util.Date;

public class NewPostBeans implements Serializable {
	private static final long serialVersionUID = 1L;

	private String subject;
	private String letter_body;
	private String category;
	private Date createdDate;
	private Date updatedDate;
	private int user;
	private int post_id;
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getLetter_body() {
		return letter_body;
	}
	public void setLetter_body(String letter_body) {
		this.letter_body = letter_body;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public int getUser() {
		return user;
	}
	public void setUser(int user) {
		this.user = user;
	}
	public int getPost_id() {
		return post_id;
	}
	public void setPost_id(int post_id) {
		this.post_id = post_id;
	}
}