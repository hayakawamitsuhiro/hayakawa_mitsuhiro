package hayakawa_mitsuhiro.beans;

import java.io.Serializable;

public class SearchBeans implements Serializable {
	private static final long serialVersionUID = 1L;

	private String search_category;
	private String created_date1;
	private String created_date2;
	public String getSearch_category() {
		return search_category;
	}
	public void setSearch_category(String search_category) {
		this.search_category = search_category;
	}
	public String getCreated_date1() {
		return created_date1;
	}
	public void setCreated_date1(String created_date1) {
		this.created_date1 = created_date1;
	}
	public String getCreated_date2() {
		return created_date2;
	}
	public void setCreated_date2(String created_date2) {
		this.created_date2 = created_date2;
	}

}