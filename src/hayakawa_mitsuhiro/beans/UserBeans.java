package hayakawa_mitsuhiro.beans;

import java.io.Serializable;
import java.util.Date;

public class UserBeans implements Serializable {
	private static final long serialVersionUID = 1L;

	private String login_id;
	private String password;
	private String confirmPassword;
	private String name;
	private int branch_office;
	private int department;
	private String office_name;
	private String department_name;
	private int user;
	private Date createdDate;
	private Date updatedDate;
	private int stop_flag;

	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmpassword) {
		this.confirmPassword = confirmpassword;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBranch_office() {
		return branch_office;
	}
	public void setBranch_office(int branch_office) {
		this.branch_office = branch_office;
	}
	public int getDepartment() {
		return department;
	}
	public void setDepartment(int department) {
		this.department = department;
	}
	public int getUser() {
		return user;
	}
	public void setUser(int user) {
		this.user = user;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public int getStop_flag() {
		return stop_flag;
	}
	public void setStop_flag(int stop_flag) {
		this.stop_flag = stop_flag;
	}
	public String getOffice_name() {
		return office_name;
	}
	public void setOffice_name(String office_name) {
		this.office_name = office_name;
	}
	public String getDepartment_name() {
		return department_name;
	}
	public void setDepartment_name(String department_name) {
		this.department_name = department_name;
	}
}
