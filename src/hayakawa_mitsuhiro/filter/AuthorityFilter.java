package hayakawa_mitsuhiro.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import hayakawa_mitsuhiro.beans.UserBeans;

@WebFilter({"/usermanagement", "/useredit", "/signup"} )
public class AuthorityFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		List<String> messages = new ArrayList<String>();
		HttpSession session = ((HttpServletRequest)request).getSession();
		UserBeans loginUser = (UserBeans) session.getAttribute("loginUser");

		if (loginUser == null) {
			messages.add("ログインしてください");
			session.setAttribute("errorMessages", messages);
			HttpServletResponse redirect = ((HttpServletResponse)response);
			redirect.sendRedirect("login");
		} else if (loginUser.getBranch_office() !=1 && loginUser.getDepartment() != 1) {
			messages.add("アクセス権限がありません");
			session.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("./").forward(request, response);
		}else{
			chain.doFilter(request, response); // サーブレットを実行
		}
	}


	public void init(FilterConfig config) throws ServletException{
	}
	public void destroy(){
	}
}