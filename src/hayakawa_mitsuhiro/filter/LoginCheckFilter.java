package hayakawa_mitsuhiro.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import hayakawa_mitsuhiro.beans.UserBeans;
@WebFilter("/*")
public class LoginCheckFilter implements Filter{
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,ServletException{
		String url = ((HttpServletRequest)req).getServletPath();
		List<String> messages = new ArrayList<String>();
		HttpSession session = ((HttpServletRequest)req).getSession();
		UserBeans loginuser = (UserBeans) session.getAttribute("loginUser");

		if(loginuser != null){
			chain.doFilter(req, res);
		}else if ("/login".equals(url) || url.matches(".*css.*")) {
				chain.doFilter(req, res);
				return;
		} else {
			messages.add("ログインしてください");
			session.setAttribute("errorMessages", messages);
			HttpServletResponse redirect = ((HttpServletResponse)res);
			redirect.sendRedirect("login");
		}

	}

	public void init(FilterConfig config) throws ServletException{
	}
	public void destroy(){
	}
}