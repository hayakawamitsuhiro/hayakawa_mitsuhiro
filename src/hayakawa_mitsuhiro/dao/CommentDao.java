package hayakawa_mitsuhiro.dao;

import static hayakawa_mitsuhiro.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import hayakawa_mitsuhiro.beans.CommentBeans;
import hayakawa_mitsuhiro.exception.SQLRuntimeException;

public class CommentDao {

	public void insert(Connection connection, CommentBeans message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append("comment");
			sql.append(", user");
			sql.append(", comment_id");
			sql.append(", post_id");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append(" ?"); // comment
			sql.append(", ?"); // user
			sql.append(", ?"); // comment_id
			sql.append(", ?"); // post_id
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, message.getComment());
			ps.setInt(2, message.getUser());
			ps.setInt(3, message.getComment_id());
			ps.setInt(4, message.getPost_id());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public List<CommentBeans> getUserMessages(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.post_id as post_id, ");
			sql.append("comments.comment_id as comment_id, ");
			sql.append("users.name as name, ");
			sql.append("comments.user as user, ");
			sql.append("comments.comment as comment, ");
			sql.append("comments.created_date as created_date ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user = users.user ");
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<CommentBeans> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}
	private List<CommentBeans> toUserMessageList(ResultSet rs)
			throws SQLException {

		List<CommentBeans> ret = new ArrayList<CommentBeans>();
		try {
			while (rs.next()) {
				int postId = rs.getInt("post_id");
				int commentId = rs.getInt("comment_id");
				int user = rs.getInt("user");
				String name = rs.getString("name");
				String comment = rs.getString("comment");
				Timestamp createdDate = rs.getTimestamp("created_date");

				CommentBeans message = new CommentBeans();
				message.setPost_id(postId);
				message.setComment_id(commentId);
				message.setUser(user);
				message.setName(name);
				message.setComment(comment);
				message.setCreated_date(createdDate);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}