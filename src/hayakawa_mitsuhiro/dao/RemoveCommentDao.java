package hayakawa_mitsuhiro.dao;

import static hayakawa_mitsuhiro.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import hayakawa_mitsuhiro.exception.SQLRuntimeException;

public class RemoveCommentDao {

	public void Delete(Connection connection, int comment) {
		PreparedStatement ps = null;
		try {
			String sql = "DELETE FROM comments WHERE comment_id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, comment);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}