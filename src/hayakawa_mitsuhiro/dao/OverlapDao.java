package hayakawa_mitsuhiro.dao;

import static hayakawa_mitsuhiro.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import hayakawa_mitsuhiro.beans.UserBeans;
import hayakawa_mitsuhiro.exception.SQLRuntimeException;

public class OverlapDao  {
	public UserBeans select(Connection connection, String id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users where user = ?;";

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, id);

			ResultSet rs = ps.executeQuery();
			UserBeans ret = toUserList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private UserBeans toUserList(ResultSet rs)
			throws SQLException {

		List<UserBeans> ret = new ArrayList<>();
		try {
			while (rs.next()) {
				UserBeans users = new UserBeans();
				String loginId = rs.getString("login_id");
				int user = rs.getInt("user");
				int stopFlag = rs.getInt("stop_flag");

				users.setLogin_id(loginId);
				users.setUser(user);
				users.setStop_flag(stopFlag);

				ret.add(users);
			}
			if (ret.size() == 1) {
				return ret.get(0);
			} else {
			return null;
			}
		} finally {
			close(rs);
		}
	}

	public UserBeans selectwhere(Connection connection, String loginId) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users where login_id = ?;";

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, loginId);

			ResultSet rs = ps.executeQuery();
			List<UserBeans> ret = List(rs);
			if (ret.isEmpty() == true) {
				return null;
			} else {
				return ret.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<UserBeans> List(ResultSet rs)
			throws SQLException {

		List<UserBeans> ret = new ArrayList<>();
		UserBeans buff = new UserBeans();
		try {
			while (rs.next()) {
				String loginId = rs.getString("login_id");
				int user = rs.getInt("user");
				int stopFlag = rs.getInt("stop_flag");

				buff.setLogin_id(loginId);
				buff.setUser(user);
				buff.setStop_flag(stopFlag);
				ret.add(buff);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
