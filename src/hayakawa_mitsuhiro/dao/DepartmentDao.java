package hayakawa_mitsuhiro.dao;

import static hayakawa_mitsuhiro.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import hayakawa_mitsuhiro.beans.UserBeans;
import hayakawa_mitsuhiro.exception.SQLRuntimeException;

public class DepartmentDao  {
	public List<UserBeans> select(Connection connection) {

		PreparedStatement ps = null;
		try {
			String sql = "select * from department;";

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserBeans> ret = toUserList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<UserBeans> toUserList(ResultSet rs)
			throws SQLException {

		List<UserBeans> ret = new ArrayList<UserBeans>();
		try {
			while (rs.next()) {
				int departmentId = rs.getInt("department_id");
				String departmentName = rs.getString("department_name");

				UserBeans departmentList = new UserBeans();
				departmentList.setDepartment(departmentId);
				departmentList.setDepartment_name(departmentName);
				ret.add(departmentList);

			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
