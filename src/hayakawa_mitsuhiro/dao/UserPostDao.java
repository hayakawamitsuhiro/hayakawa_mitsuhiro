package hayakawa_mitsuhiro.dao;

import static hayakawa_mitsuhiro.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import hayakawa_mitsuhiro.beans.UserPostBeans;
import hayakawa_mitsuhiro.exception.SQLRuntimeException;

public class UserPostDao {

	public List<UserPostBeans> getUserMessages(Connection connection, int num, String searchDate1, String searchDate2,
			String searchCategory) {
		StringBuilder sql = new StringBuilder();
		PreparedStatement ps = null;
		try {
			if (StringUtils.isEmpty(searchDate1) && StringUtils.isEmpty(searchDate2) && StringUtils.isEmpty(searchCategory)) {
				sql.append("SELECT ");
				sql.append("posts.user as user, ");
				sql.append("posts.subject as subject, ");
				sql.append("posts.letter_body as letter_body, ");
				sql.append("posts.category as category, ");
				sql.append("posts.post_id as post_id, ");
				sql.append("users.login_id as login_id, ");
				sql.append("users.name as name, ");
				sql.append("posts.created_date as created_date ");
				sql.append("FROM posts ");
				sql.append("INNER JOIN users ");
				sql.append("ON posts.user = users.user ");
				sql.append("ORDER BY created_date DESC limit " + num);

				ps = connection.prepareStatement(sql.toString());
				ResultSet rs = ps.executeQuery();
				List<UserPostBeans> ret = toUserMessageList(rs);
				return ret;
			} else {
				sql.append("SELECT ");
				sql.append("posts.user as user, ");
				sql.append("posts.subject as subject, ");
				sql.append("posts.letter_body as letter_body, ");
				sql.append("posts.category as category, ");
				sql.append("posts.post_id as post_id, ");
				sql.append("users.login_id as login_id, ");
				sql.append("users.name as name, ");
				sql.append("posts.created_date as created_date ");
				sql.append("FROM posts ");
				sql.append("INNER JOIN users ");
				sql.append("ON posts.user = users.user ");
				sql.append("where category LIKE ? AND posts.created_date BETWEEN ? AND ?");
				sql.append("ORDER BY created_date DESC limit " + num);

				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, "%"+ searchCategory +"%");
				ps.setString(2, searchDate1 + "\t00:00:00");
				ps.setString(3, searchDate2 + "\t23:00:00");

				ResultSet rs = ps.executeQuery();
				List<UserPostBeans> ret = toUserMessageList(rs);
				return ret;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
	private List<UserPostBeans> toUserMessageList(ResultSet rs)
			throws SQLException {

		List<UserPostBeans> ret = new ArrayList<UserPostBeans>();
		try {
			while (rs.next()) {
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				int user = rs.getInt("user");
				int postId = rs.getInt("post_id");
				String subject = rs.getString("subject");
				String letter_body = rs.getString("letter_body");
				String category = rs.getString("category");
				Date createdDate = rs.getTimestamp("created_date");

				UserPostBeans message = new UserPostBeans();
				message.setLogin_id(loginId);
				message.setName(name);
				message.setUser(user);
				message.setPost_id(postId);
				message.setSubject(subject);
				message.setLetter_body(letter_body);
				message.setCategory(category);
				message.setCreated_date(createdDate);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}