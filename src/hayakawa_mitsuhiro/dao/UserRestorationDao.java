package hayakawa_mitsuhiro.dao;

import static hayakawa_mitsuhiro.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import hayakawa_mitsuhiro.exception.SQLRuntimeException;

public class UserRestorationDao  {

	public void update(Connection connection, int user) {

		PreparedStatement ps = null;
		try {

				String sql = "UPDATE users set stop_flag = 0 where user = ?;";


				ps = connection.prepareStatement(sql.toString());

				ps.setInt(1, user);
				ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
