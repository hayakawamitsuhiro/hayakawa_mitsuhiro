package hayakawa_mitsuhiro.dao;

import static hayakawa_mitsuhiro.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.apache.commons.lang.StringUtils;

import hayakawa_mitsuhiro.beans.UserBeans;
import hayakawa_mitsuhiro.exception.SQLRuntimeException;

public class UserEditDao  {
	public UserBeans select(Connection connection, int user) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users INNER JOIN department ON users.department = department.department_id INNER JOIN office ON users.branch_office = office.office_id where users.user=?;";

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, user);

			ResultSet rs = ps.executeQuery();
			UserBeans ret = toUserList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private UserBeans toUserList(ResultSet rs)
			throws SQLException {

		UserBeans ret = new UserBeans();
		try {
			while (rs.next()) {
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				String branchOfficeName = rs.getString("office_name");
				String departmentName = rs.getString("department_name");
				int user = rs.getInt("user");
				int departmentId = rs.getInt("department");
				int branchOfficeId = rs.getInt("branch_office");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");

				ret.setLogin_id(loginId);
				ret.setPassword(password);
				ret.setName(name);
				ret.setOffice_name(branchOfficeName);
				ret.setDepartment_name(departmentName);
				ret.setUser(user);
				ret.setCreatedDate(createdDate);
				ret.setUpdatedDate(updatedDate);
				ret.setBranch_office(branchOfficeId);
				ret.setDepartment(departmentId);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
	public void update(Connection connection, UserBeans user) {

		PreparedStatement ps = null;
		try {
			if (StringUtils.isEmpty(user.getPassword())){

				String sql = "UPDATE users set login_id = ?, name = ?, branch_office = ?, department = ?, updated_date = CURRENT_TIMESTAMP where user = ?;";


				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, user.getLogin_id());
				ps.setString(2, user.getName());
				ps.setInt(3, user.getBranch_office());
				ps.setInt(4, user.getDepartment());
				ps.setInt(5, user.getUser());
				ps.executeUpdate();
			} else {
				String sql = "UPDATE users set login_id = ?, name = ?, password = ?, branch_office = ?, department = ?, updated_date = CURRENT_TIMESTAMP where user = ?;";


				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, user.getLogin_id());
				ps.setString(2, user.getName());
				ps.setString(3, user.getPassword());
				ps.setInt(4, user.getBranch_office());
				ps.setInt(5, user.getDepartment());
				ps.setInt(6, user.getUser());
				ps.executeUpdate();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
