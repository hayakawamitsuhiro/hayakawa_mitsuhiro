package hayakawa_mitsuhiro.dao;

import static hayakawa_mitsuhiro.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import hayakawa_mitsuhiro.beans.UserBeans;
import hayakawa_mitsuhiro.exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, UserBeans user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("login_id");
			sql.append(", password");
			sql.append(", name");
			sql.append(", branch_office");
			sql.append(", department");
			sql.append(", user");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append("?"); // loginId
			sql.append(", ?"); // password
			sql.append(", ?"); // name
			sql.append(", ?"); // branchOffice
			sql.append(", ?"); // department
			sql.append(", ?"); // user
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLogin_id());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranch_office());
			ps.setInt(5, user.getDepartment());
			ps.setInt(6, user.getUser());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public UserBeans getUser(Connection connection, String login_id,
			String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, login_id);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<UserBeans> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<UserBeans> toUserList(ResultSet rs) throws SQLException {

		List<UserBeans> ret = new ArrayList<UserBeans>();
		try {
			while (rs.next()) {
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branchOffice = rs.getInt("branch_office");
				int department = rs.getInt("department");
				int user = rs.getInt("user");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");
				int flag = rs.getInt("stop_flag");

				UserBeans users = new UserBeans();
				users.setLogin_id(loginId);
				users.setPassword(password);;
				users.setName(name);
				users.setBranch_office(branchOffice);
				users.setDepartment(department);
				users.setUser(user);
				users.setCreatedDate(createdDate);
				users.setUpdatedDate(updatedDate);
				users.setStop_flag(flag);

				ret.add(users);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}