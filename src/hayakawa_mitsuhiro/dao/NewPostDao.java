package hayakawa_mitsuhiro.dao;

import static hayakawa_mitsuhiro.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import hayakawa_mitsuhiro.beans.NewPostBeans;
import hayakawa_mitsuhiro.exception.SQLRuntimeException;

public class NewPostDao {

	public void insert(Connection connection, NewPostBeans message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO posts ( ");
			sql.append("user");
			sql.append(", subject");
			sql.append(", letter_body");
			sql.append(", category");
			sql.append(", post_id");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append(" ?"); // user
			sql.append(", ?"); // subject
			sql.append(", ?"); // letter_body
			sql.append(", ?"); // category
			sql.append(", ?"); //post_id
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());


			ps.setInt(1, message.getUser());
			ps.setString(2, message.getSubject());
			ps.setString(3, message.getLetter_body());
			ps.setString(4, message.getCategory());
			ps.setInt(5, message.getPost_id());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}