package hayakawa_mitsuhiro.dao;

import static hayakawa_mitsuhiro.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import hayakawa_mitsuhiro.exception.SQLRuntimeException;

public class RemovePostDao {

	public void Delete(Connection connection, int post) {
		PreparedStatement ps = null;
		try {
			String sql = "DELETE FROM posts WHERE post_id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, post);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}