package hayakawa_mitsuhiro.dao;

import static hayakawa_mitsuhiro.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import hayakawa_mitsuhiro.beans.UserBeans;
import hayakawa_mitsuhiro.exception.SQLRuntimeException;

public class UserManagementDao  {
	public List<UserBeans> select(Connection connection) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users INNER JOIN department ON users.department = department.department_id INNER JOIN office ON users.branch_office = office.office_id order by created_date desc;";

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserBeans> ret = toUserList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<UserBeans> toUserList(ResultSet rs)
			throws SQLException {

		List<UserBeans> ret = new ArrayList<UserBeans>();
		try {
			while (rs.next()) {
				String name = rs.getString("name");
				String loginId = rs.getString("login_id");
				String department = rs.getString("department_name");
				String branchOffice = rs.getString("office_name");
				Timestamp createdDate = rs.getTimestamp("created_date");
				int user = rs.getInt("user");
				int flag = rs.getInt("stop_flag");

				UserBeans users = new UserBeans();
				users.setName(name);
				users.setLogin_id(loginId);
				users.setDepartment_name(department);
				users.setOffice_name(branchOffice);
				users.setCreatedDate(createdDate);
				users.setUser(user);
				users.setStop_flag(flag);

				ret.add(users);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
