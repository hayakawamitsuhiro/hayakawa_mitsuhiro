package hayakawa_mitsuhiro.dao;

import static hayakawa_mitsuhiro.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import hayakawa_mitsuhiro.exception.SQLRuntimeException;

public class UserRemoveDao {

	public void remove(Connection connection, int user) {
		PreparedStatement ps = null;
		try {
			String sql = "DELETE FROM users WHERE user = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, user);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}