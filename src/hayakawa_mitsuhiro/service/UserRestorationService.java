package hayakawa_mitsuhiro.service;

import static hayakawa_mitsuhiro.utils.CloseableUtil.*;
import static hayakawa_mitsuhiro.utils.DBUtil.*;

import java.sql.Connection;

import hayakawa_mitsuhiro.dao.UserRestorationDao;

public class UserRestorationService {

	public void restoration(int user) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserRestorationDao stopDao = new UserRestorationDao();
			stopDao.update(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}