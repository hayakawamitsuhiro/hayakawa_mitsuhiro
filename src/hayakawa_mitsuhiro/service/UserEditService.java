package hayakawa_mitsuhiro.service;

import static hayakawa_mitsuhiro.utils.CloseableUtil.*;
import static hayakawa_mitsuhiro.utils.DBUtil.*;

import java.sql.Connection;

import org.apache.commons.lang.StringUtils;

import hayakawa_mitsuhiro.beans.UserBeans;
import hayakawa_mitsuhiro.dao.UserEditDao;
import hayakawa_mitsuhiro.utils.CipherUtil;

public class UserEditService {

	public  UserBeans user(int user) {

		Connection connection = null;
		try {
			connection = getConnection();


			UserEditDao userEditDao = new UserEditDao();
			UserBeans ret = userEditDao.select(connection, user);


			commit(connection);
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}

	public void edituser(UserBeans user) {

		Connection connection = null;
		try {
			connection = getConnection();

			if (StringUtils.isEmpty(user.getPassword())){
				UserEditDao userDao = new UserEditDao();
				userDao.update(connection, user);

				commit(connection);
			} else {

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserEditDao userDao = new UserEditDao();
			userDao.update(connection, user);

			commit(connection);
			}
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}
}