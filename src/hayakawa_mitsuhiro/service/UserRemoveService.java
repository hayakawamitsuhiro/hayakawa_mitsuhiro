package hayakawa_mitsuhiro.service;

import static hayakawa_mitsuhiro.utils.CloseableUtil.*;
import static hayakawa_mitsuhiro.utils.DBUtil.*;

import java.sql.Connection;

import hayakawa_mitsuhiro.dao.UserRemoveDao;

public class UserRemoveService {

	public void remove(int user) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserRemoveDao userRemoveDao = new UserRemoveDao();
			userRemoveDao.remove(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}