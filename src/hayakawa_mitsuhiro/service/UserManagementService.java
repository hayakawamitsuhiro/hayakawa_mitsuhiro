package hayakawa_mitsuhiro.service;

import static hayakawa_mitsuhiro.utils.CloseableUtil.*;
import static hayakawa_mitsuhiro.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import hayakawa_mitsuhiro.beans.UserBeans;
import hayakawa_mitsuhiro.dao.UserManagementDao;

public class UserManagementService {

	public  List<UserBeans> userList() {

		Connection connection = null;
		try {
			connection = getConnection();


			UserManagementDao userDao = new UserManagementDao();
			List<UserBeans> ret = userDao.select(connection);


			commit(connection);
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}
}