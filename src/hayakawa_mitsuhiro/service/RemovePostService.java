package hayakawa_mitsuhiro.service;

import static hayakawa_mitsuhiro.utils.CloseableUtil.*;
import static hayakawa_mitsuhiro.utils.DBUtil.*;

import java.sql.Connection;

import hayakawa_mitsuhiro.dao.RemovePostDao;

public class RemovePostService {

	public void delete(int message) {

		Connection connection = null;
		try {
			connection = getConnection();

			RemovePostDao removeDao = new RemovePostDao();
			removeDao.Delete(connection, message);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}