package hayakawa_mitsuhiro.service;

import static hayakawa_mitsuhiro.utils.CloseableUtil.*;
import static hayakawa_mitsuhiro.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import hayakawa_mitsuhiro.beans.UserBeans;
import hayakawa_mitsuhiro.dao.OfficeDao;

public class OfficeService {

	public  List<UserBeans> officeList() {

		Connection connection = null;
		try {
			connection = getConnection();


			OfficeDao officeDao = new OfficeDao();
			List<UserBeans> ret = officeDao.select(connection);


			commit(connection);
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}
}