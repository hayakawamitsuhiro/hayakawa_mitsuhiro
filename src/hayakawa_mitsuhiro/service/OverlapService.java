package hayakawa_mitsuhiro.service;

import static hayakawa_mitsuhiro.utils.CloseableUtil.*;
import static hayakawa_mitsuhiro.utils.DBUtil.*;

import java.sql.Connection;

import hayakawa_mitsuhiro.beans.UserBeans;
import hayakawa_mitsuhiro.dao.OverlapDao;

public class OverlapService {

	public  UserBeans userList(String id) {

		Connection connection = null;
		try {
			connection = getConnection();


			OverlapDao userDao = new OverlapDao();
			 UserBeans ret = userDao.select(connection, id);


			commit(connection);
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}
	public  UserBeans List(String loginId) {

		Connection connection = null;
		try {
			connection = getConnection();


			OverlapDao userDao = new OverlapDao();
			 UserBeans ret = userDao.selectwhere(connection, loginId);


			commit(connection);
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}