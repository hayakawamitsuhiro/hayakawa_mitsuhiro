package hayakawa_mitsuhiro.service;

import static hayakawa_mitsuhiro.utils.CloseableUtil.*;
import static hayakawa_mitsuhiro.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import hayakawa_mitsuhiro.beans.NewPostBeans;
import hayakawa_mitsuhiro.beans.UserPostBeans;
import hayakawa_mitsuhiro.dao.NewPostDao;
import hayakawa_mitsuhiro.dao.UserPostDao;

public class NewPostService {

	public void register(NewPostBeans message) {

		Connection connection = null;
		try {
			connection = getConnection();

			NewPostDao newPostDao = new NewPostDao();
			newPostDao.insert(connection, message);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	private static final int LIMIT_NUM = 1000;

	public List<UserPostBeans> getMessage(String Date1, String Date2, String Category) {

		Connection connection = null;

		try {
			connection = getConnection();

			UserPostDao messageDao = new UserPostDao();
			List<UserPostBeans> ret = messageDao.getUserMessages(connection, LIMIT_NUM, Date1, Date2, Category);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}