package hayakawa_mitsuhiro.service;

import static hayakawa_mitsuhiro.utils.CloseableUtil.*;
import static hayakawa_mitsuhiro.utils.DBUtil.*;

import java.sql.Connection;

import hayakawa_mitsuhiro.beans.UserBeans;
import hayakawa_mitsuhiro.dao.UserDao;
import hayakawa_mitsuhiro.utils.CipherUtil;

public class LoginService {

	public UserBeans login(String login_id, String password) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			String encPassword = CipherUtil.encrypt(password);
			UserBeans user = userDao.getUser(getConnection(), login_id, encPassword);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}